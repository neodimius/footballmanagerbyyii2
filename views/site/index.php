<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You in my first Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="index.php?r=football">Open Football Manager</a></p>
    </div>

    <div class="body-content">
        
    </div>
</div>
