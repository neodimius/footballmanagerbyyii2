<?php
    namespace app\controllers;

    use Yii;
    use yii\web\Controller;
    use yii\web\Request;

    use yii\data\Pagination;

    use app\models\Teams;
    use app\models\Players;
    use app\models\AddTeamForm;
    use app\models\AddPlayerForm;
    use app\models\EditTeamForm;
    use app\models\EditPlayerForm;

    class FootballController extends Controller {
    /*All Teams */
        public function actionIndex() {

            $query = Teams::find();

            $pagination = new Pagination ([
                'defaultPageSize' => 5,
                'totalCount' => $query->count(),
            ]);

                
            $teams = $query -> orderBy('id')
                -> offset($pagination->offset)    //Query of teams table
                -> limit ($pagination->limit)
                -> all();

                // Render Teams table
            return $this -> render('index.twig', [
                'teams' => $teams,
                'pagination' => $pagination,
            ]);
        }

    /*Get Players of Team*/
        public function actionTeam() {

            $query = Players::find();

            $request = Yii::$app->request;
            $team = $request->get('team');  // Get Team Name from $_GET

            $players = $query->where(['teamName' => "$team"])     //Query of players from Team
                -> all();

            return $this->render( 'team.twig', ['players' => $players, 'team' => $team] );
        }
    
    /* Universal Action for Deleting Team or Player  */
        public function actionDel() {

            $request = Yii::$app->request;
            
            $table = $request->get('table');
            $id = $request->get('id');
            $team = $request->get('team');

            if ($table == "Teams"){
                $query = Teams::findOne($id);
                $query->delete();
                
                return $this->redirect('index.php?r=football');
            } else if ($table == "Players"){
                $query = Players::findOne($id);
                $query->delete();
                $location = "index.php?r=football%2Fteam&team=$team";
        
                return $this->redirect($location);
            }
        }

    /* Add New Team */    
        public function actionAddTeam() {
            
            $form = new AddTeamForm();

            if ( $form->load(Yii::$app->request->post()) && $form->validate() ) {
               
                // insert a new row (new Team)
                $team = new Teams();
                $team->name = $form->teamname;
                $team->year = $form->teamyear;
                $team->save();
                
                return $this->redirect('index.php?r=football');

            } else {

                return $this->render('addTeam.twig', ['form' => $form]);

            }
        }

    /* Add New Player to the opened Team*/
        public function actionAddPlayer() {
            
            $form = new AddPlayerForm();
            $team = Yii::$app->request->get('team');

            if ( $form->load(Yii::$app->request->post()) &&  $form->validate() ) {
                
                // insert a new row (new Player)
                $player = new Players();
                
                $player->teamName = $team;
                $player->name = $form->name;
                $player->lastName = $form->lastname;
                $player->birth = $form->birth;
                $player->position = $form->position;

                $player->save();

                return $this->redirect('index.php?r=football%2Fteam&team=' . $team);

            } else {

                return $this->render('addPlayer.twig', ['form' => $form, 'team' => $team]);

            }

        }

    /* Edit Team */    
        public function actionEditTeam() {
            
            $id = Yii::$app->request->get('teamid');

            $team = Teams::findOne($id);
    
            $form = new EditTeamForm();

            if ( $form->load(Yii::$app->request->post()) && $form->validate() ) {
               
                // Update the Team 
                $team = Teams::findOne($form->teamid);
                $team->name = $form->teamname;
                $team->year = $form->teamyear;
                $team->save();
                
                return $this->redirect('index.php?r=football');

            } else {
                
                // Put data from DB to form
                $form->teamname = $team->name;
                $form->teamyear = $team->year;
                $form->teamid = $team->id;

                return $this->render('editTeam.twig', ['form' => $form]);

            }
        }


    /* Edit Player */    
        public function actionEditPlayer() {
            $id = Yii::$app->request->get('playerid');

            $player = Players::findOne($id);
    
            $form = new EditPlayerForm();

            if ( $form->load(Yii::$app->request->post()) && $form->validate() ) {
               
                // Update the Player Data
                $player = Players::findOne($form->id);
                $player->name = $form->name;
                $player->lastName = $form->lastname;
                $player->birth = $form->birth;
                $player->position = $form->position;
                $player->teamName = $form->teamname;
                $player->save();
                
                return $this->redirect('index.php?r=football%2Fteam&team='. $form->teamname);

            } else {
                
                // Put data from DB to form
                $form->name = $player->name;
                $form->lastname = $player->lastName;
                $form->birth = $player->birth;
                $form->position = $player->position;
                $form->teamname = $player->teamName;
                $form->id = $player->id;

                return $this->render('editPlayer.twig', ['form' => $form]);

            }
        }

    }
?>