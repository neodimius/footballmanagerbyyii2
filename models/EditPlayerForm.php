<?php

namespace app\models;

use Yii;
use yii\base\Model;

class EditPlayerForm extends \yii\base\Model {

    public $name;
    public $birth;
    public $lastname;
    public $position;
    public $id;
    public $teamname;

    public function rules() {
        
        return [

            [['name', 'lastname', 'birth', 'position', 'teamname', 'id'], 'required'],
            ['lastname', 'string', 'min' => 2, 'max' => 30],
            ['name', 'string', 'min' => 2, 'max' => 15],
            ['birth', 'number', 'min' => 1940, 'max' => 2018],
            ['lastname', 'string', 'min' => 2, 'max' => 15],
            ['position', 'string', 'min' => 2, 'max' => 15],
    
        ];

    }

}

?>