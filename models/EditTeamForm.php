<?php
namespace app\models;

use Yii;
use yii\base\Model;

class EditTeamForm extends \yii\base\Model {

    public $teamname;
    public $teamyear;
    public $teamid;

    public function rules() {

        return [

            [['teamname', 'teamyear', 'teamid'], 'required'],
            ['teamname', 'string', 'min' => 3, 'max' => 30],
            ['teamyear', 'number', 'min' => 1800, 'max' => 2018],
            ['teamid', 'string', 'max' => 4],

        ];

    }

}

?>