<?php
namespace app\models;

use Yii;
use yii\base\Model;

class AddTeamForm extends \yii\base\Model {

    public $teamname;
    public $teamyear;

    public function rules() {

        return [

            [['teamname', 'teamyear'], 'required'],
            ['teamname', 'string', 'min' => 3, 'max' => 30],
            ['teamyear', 'number', 'min' => 1800, 'max' => 2018],

        ];

    }

}

?>