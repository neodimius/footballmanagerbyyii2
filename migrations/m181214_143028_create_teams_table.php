<?php

use yii\db\Migration;

/**
 * Handles the creation of table `teams`.
 */
class m181214_143028_create_teams_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('teams', [
            'id' => $this->primaryKey(),
            'name' => $this->string(30),
            'year' => $this->integer(4),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('teams');
    }
}
