<?php

use yii\db\Migration;

/**
 * Handles the creation of table `players`.
 */
class m181214_143057_create_players_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('players', [
            'id' => $this->primaryKey(),
            'teamName' => $this->string(30),
            'name' => $this->string(15),
            'lastName' => $this->string(15),
            'birth' => $this->integer(10),
            'position' => $this->string(15),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('players');
    }
}
